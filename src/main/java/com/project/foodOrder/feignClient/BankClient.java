package com.project.foodOrder.feignClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.project.foodOrder.dto.TransferDetails;


@FeignClient(name = "http://BANK-SERVICE/bank/api/account")
public interface BankClient {
	@PostMapping("/fundtransfer")
	public String transfer(@RequestBody TransferDetails transferDetails);
}

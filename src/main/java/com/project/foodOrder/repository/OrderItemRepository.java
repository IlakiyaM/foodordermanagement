package com.project.foodOrder.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.foodOrder.entity.Order;

public interface OrderItemRepository extends JpaRepository<Order, Long>{

}

package com.project.foodOrder.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.project.foodOrder.dto.SearchResponseDTO;
import com.project.foodOrder.entity.FoodProduct;
import com.project.foodOrder.entity.OrderDetails;

@Repository
public interface FoodOrderRepository extends JpaRepository<FoodProduct, Long> {

	@Query("select new com.project.foodOrder.dto.SearchResponseDTO(foodItemId,foodItemName,price,vendorId,vendorName) from FoodProduct where foodItemName like %:foodItemName%")
	List<SearchResponseDTO> searchFoodProduct(@Param("foodItemName") String foodItemName);

	FoodProduct findByFoodItemId(Long foodItemId);

}

package com.project.foodOrder.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.foodOrder.dto.OrderRequest;
import com.project.foodOrder.dto.SearchResponseDTO;
import com.project.foodOrder.entity.OrderDetails;
import com.project.foodOrder.feignClient.BankClient;
import com.project.foodOrder.service.FoodOrderService;

@RestController
@RequestMapping("/foodOrder")

public class FoodOrderController {
	@Autowired
	private FoodOrderService foodOrderService;

	@GetMapping("/search")
	public List<SearchResponseDTO> searchFoodProduct(@RequestParam String foodItem) {
		return foodOrderService.searchFoodProduct(foodItem);
	}

	@PostMapping("/placeOrder")
	public String placeOrder(@RequestBody OrderRequest orderRequest) {
		String response = foodOrderService.placeOrder(orderRequest);
		return response;
	}

	@GetMapping("/orderHistory")
	public List<OrderDetails> orderHistory(@RequestParam Long userId) {
		return foodOrderService.orderHistory(userId);
	}
}

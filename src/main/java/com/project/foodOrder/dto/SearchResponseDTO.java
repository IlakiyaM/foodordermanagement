package com.project.foodOrder.dto;

public class SearchResponseDTO {
	
	public SearchResponseDTO(String foodItemName){
		this.foodItemName = foodItemName;
	}
	private Long foodItemId;
	private String foodItemName;
	private Double price;
	private Long vendorId;
	private String vendorName;
	public Long getFoodItemId() {
		return foodItemId;
	}
	public SearchResponseDTO(Long foodItemId, String foodItemName, Double price, Long vendorId, String vendorName) {
		super();
		this.foodItemId = foodItemId;
		this.foodItemName = foodItemName;
		this.price = price;
		this.vendorId = vendorId;
		this.vendorName = vendorName;
	}
	public void setFoodItemId(Long foodItemId) {
		this.foodItemId = foodItemId;
	}
	public String getFoodItemName() {
		return foodItemName;
	}
	public void setFoodItemName(String foodItemName) {
		this.foodItemName = foodItemName;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Long getVendorId() {
		return vendorId;
	}
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
}

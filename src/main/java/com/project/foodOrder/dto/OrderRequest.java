package com.project.foodOrder.dto;

import java.util.List;

import com.project.foodOrder.entity.Order;

public class OrderRequest {

	private Long userId;

	private Long fromAccount;

	private List<OrderItemRequest> order;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getFromAccount() {
		return fromAccount;
	}

	public void setFromAccount(Long fromAccount) {
		this.fromAccount = fromAccount;
	}

	public List<OrderItemRequest> getOrder() {
		return order;
	}

	public void setOrder(List<OrderItemRequest> order) {
		this.order = order;
	}

}

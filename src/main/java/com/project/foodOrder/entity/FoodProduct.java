package com.project.foodOrder.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "food_products")
public class FoodProduct implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "food_item_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long foodItemId;

	@Column(name = "food_item_name")
	private String foodItemName;

	@Column(name = "price")
	private double price;

	@Column(name = "vendor_id")
	private Long vendorId;

	@Column(name = "vendor_name")
	private String vendorName;

	public Long getFoodItemId() {
		return foodItemId;
	}

	public void setFoodItemId(Long foodItemId) {
		this.foodItemId = foodItemId;
	}

	public String getFoodItemName() {
		return foodItemName;
	}

	public FoodProduct() {
	}

	public FoodProduct(Long foodItemId, String foodItemName, double price, Long vendorId, String vendorName) {
		super();
		this.foodItemId = foodItemId;
		this.foodItemName = foodItemName;
		this.price = price;
		this.vendorId = vendorId;
		this.vendorName = vendorName;
	}

	public void setFoodItemName(String foodItemName) {
		this.foodItemName = foodItemName;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
}

package com.project.foodOrder.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "order_items")
public class Order {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "order_item_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long orderItemId;

	@Column(name = "order_id")
	private Long orderId;

	@Column(name = "food_item_id")
	private Long foodItemId;

	@Column(name = "vendor_id")
	private Long vendorId;

	@Column(name = "quantity")
	private Long quantity;

	public Long getOrderItemId() {
		return orderItemId;
	}

	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public Long getFoodItemId() {
		return foodItemId;
	}

	public void setFoodItemId(Long foodItemId) {
		this.foodItemId = foodItemId;
	}

	public Long getVendorId() {
		return vendorId;
	}

	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Order(Long orderId, Long foodItemId, Long vendorId, Long quantity) {
		this.orderId = orderId;
		this.foodItemId = foodItemId;
		this.vendorId = vendorId;
		this.quantity = quantity;
	}

	public Order() {
	}
}

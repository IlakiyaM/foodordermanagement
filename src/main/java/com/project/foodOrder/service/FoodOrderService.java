package com.project.foodOrder.service;

import java.util.List;

import com.project.foodOrder.dto.OrderRequest;
import com.project.foodOrder.dto.SearchResponseDTO;
import com.project.foodOrder.entity.OrderDetails;

public interface FoodOrderService {
	public List<SearchResponseDTO> searchFoodProduct(String foodItem);

	public String placeOrder(OrderRequest orderRequest);

	public List<OrderDetails> orderHistory(Long userId);
}

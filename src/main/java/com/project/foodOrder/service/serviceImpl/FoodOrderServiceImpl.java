package com.project.foodOrder.service.serviceImpl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.http.ResponseEntity;

import com.project.foodOrder.dto.OrderItemRequest;
import com.project.foodOrder.dto.OrderRequest;
import com.project.foodOrder.dto.SearchResponseDTO;
import com.project.foodOrder.dto.TransferDetails;
import com.project.foodOrder.entity.FoodProduct;
import com.project.foodOrder.entity.Order;
import com.project.foodOrder.entity.OrderDetails;
import com.project.foodOrder.feignClient.BankClient;
import com.project.foodOrder.repository.FoodOrderRepository;
import com.project.foodOrder.repository.OrderDetailsRepository;
import com.project.foodOrder.repository.OrderItemRepository;
import com.project.foodOrder.service.FoodOrderService;

@Service
public class FoodOrderServiceImpl implements FoodOrderService {
	@Autowired
	FoodOrderRepository foodOrderRepository;
	@Autowired
	OrderItemRepository orderItemRepository;
	@Autowired
	OrderDetailsRepository orderDetailsRepository;
	@Autowired
	BankClient bankClient;
	@Value("${toAccount.number}")
	protected Long toAccountNumber;

	@Override
	public List<SearchResponseDTO> searchFoodProduct(String foodItem) {
		return foodOrderRepository.searchFoodProduct(foodItem);

	}

	@Override
	public List<OrderDetails> orderHistory(Long userId) {
		List<OrderDetails> orderDetails = orderDetailsRepository.findByUserId(userId);
		List<OrderDetails> order = orderDetails.stream().limit(5).collect(Collectors.toList());
		return order;
	}

	@Override
	public String placeOrder(OrderRequest orderRequest) {
		List<OrderItemRequest> orderItem = orderRequest.getOrder();
		// List<Order> orderList=new ArrayList<Order>();
		double totalAmount = 0.0;
		for (OrderItemRequest orderItemRequest : orderItem) {
			double total = 0.0;
			FoodProduct foodProduct = foodOrderRepository.findByFoodItemId(orderItemRequest.getFoodItemId());
			if (orderItemRequest.getQuantity() > 0) {
				total = foodProduct.getPrice() * orderItemRequest.getQuantity();
				totalAmount += total;
			} else {
				return "Quantity should be greater than zero";
			}
		}
		TransferDetails transferDetails = new TransferDetails(orderRequest.getFromAccount(), toAccountNumber,
				totalAmount);
		String response = bankClient.transfer(transferDetails);
		if (response.equalsIgnoreCase("Success")) {
			OrderDetails orderDetails = orderDetailsRepository.save(new OrderDetails(orderRequest.getUserId(),
					totalAmount, (new Timestamp(System.currentTimeMillis())).toString()));
			orderDetailsRepository.save(orderDetails);
			for (OrderItemRequest orderItemRequest : orderItem) {
				Order order = new Order();
				order.setFoodItemId(orderItemRequest.getFoodItemId());
				order.setOrderId(orderDetails.getOrderId());
				order.setQuantity(orderItemRequest.getQuantity());
				order.setVendorId(orderItemRequest.getVendorId());
				orderItemRepository.save(order);
			}
			return "Your Order has been placed successfully";
		} else if (response.equalsIgnoreCase("Insufficient Funds.")) {
			return "Insufficient Funds.";
		} else {
			return "Couldnt complete your Order. Try after some time";
		}
	}

}
